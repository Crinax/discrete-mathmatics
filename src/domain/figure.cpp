#include <memory>
#include "figure.h"

domain::Figure::Figure(std::unique_ptr<domain::Point> center, domain::FigureType type):
  center(std::move(center)),
  _type(type)
{}

domain::Point domain::Figure::get_center() {
  return { this->center->x, this->center->y };
}

domain::FigureType domain::Figure::get_type() {
  return this->_type;
}

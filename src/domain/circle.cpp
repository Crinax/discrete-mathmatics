#include <cmath>
#include <memory>
#include "circle.h"
#include "figure.h"

domain::Circle::Circle(double radius, std::unique_ptr<domain::Point> center):
  _radius(radius),
  Figure(std::move(center), domain::FigureType::Circle)
{}

double domain::Circle::get_radius() {
  return this->_radius;
}

double domain::Circle::calc_area() {
  return std::numbers::pi * std::pow(this->_radius, 2);
}

bool domain::Circle::has_point(std::unique_ptr<domain::Point> point) {
  return std::pow(point->x - this->center->x, 2) +
    std::pow(point->y - this->center->y, 2) <= std::pow(this->_radius, 2);
}

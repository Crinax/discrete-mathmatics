#include <memory>
#include "rect.h"
#include "figure.h"

domain::Rectangle::Rectangle(
    double width,
    double height,
    std::unique_ptr<domain::Point> center
): _width(width),
  _height(height),
  Figure(std::move(center), domain::FigureType::Rectangle)
{}

double domain::Rectangle::get_height() {
  return this->_height;
}

double domain::Rectangle::get_width() {
  return this->_width;
}

double domain::Rectangle::calc_area() {
  return this->_width * this->_height;
}

bool domain::Rectangle::has_point(std::unique_ptr<Point> point) {
  return (
    point->x >= this->center->x - this->_width &&
    point->x <= this->center->x + this->_width
  ) && (
    point->y >= this->center->y - this->_height &&
    point->y <= this->center->y + this->_height
  );
}

#pragma once

#include <memory>

namespace domain {
  struct Point {
    double x;
    double y;
  };

  enum class FigureType {
    Rectangle,
    Circle
  };

  class Figure {
    public:
      Figure(std::unique_ptr<Point> center, FigureType type);

      Point get_center();
      FigureType get_type();
      
      virtual double calc_area() = 0;
      virtual bool has_point(std::unique_ptr<Point> point) = 0;

    protected:
      std::unique_ptr<Point> center;

    private:
      FigureType _type;
  };
}

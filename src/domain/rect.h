#pragma once

#include "figure.h"

namespace domain {
  class Rectangle : public Figure {
    public:
      Rectangle(double, double, std::unique_ptr<Point>);

      double get_width();
      double get_height();
      double calc_area();
      bool has_point(std::unique_ptr<Point>);

    private:
      double _width;
      double _height;
  };
}

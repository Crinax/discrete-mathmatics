#pragma once
#include "figure.h"

namespace domain {
  class Circle : public Figure {
    public:
      Circle(double, std::unique_ptr<Point>);

      double get_radius();
      double calc_area();
      bool has_point(std::unique_ptr<Point>);

    private:
      double _radius;
  };
}


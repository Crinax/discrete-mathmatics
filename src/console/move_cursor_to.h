#pragma once

#include <iostream>

namespace console {
  inline void move_cursor_to(int by_row, int by_column) {
    std::cout << "\033[" << by_row << ";" << by_column << "H";
  }
}

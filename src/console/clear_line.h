#include <iostream>

namespace console {
  inline void clear_line() {
    std::cout << "\x1b[1A\x1b[2K";
  }
}

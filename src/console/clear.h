#pragma once

#include <cstdlib>
#include <iostream>

#define IS_WINDOWS false

#ifdef _WIN32
  #define IS_WINDOWS true
#endif // _WIN32

#ifdef _WIN64
  #define IS_WINDOWS true
#endif // _WIN64

namespace console {
  inline void clear() {
    if (IS_WINDOWS) {
      system("cls");
    } else {
      system("clear");
    }

    std::flush(std::cout);
  }
}

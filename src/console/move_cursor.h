#pragma once

#include <iostream>

namespace console {
  inline void move_cursor(int by_row, int by_column) {
    char cur_v_dir = by_row < 0 ? 'B' : 'A';
    char cur_h_dir = by_column < 0 ? 'D' : 'C';

    std::cout << "\033[" << by_row << cur_v_dir << "\033[" << by_column << cur_h_dir;
  }
}

#include <memory>
#include "parser.h"

Parser::Parser() {}

template<> std::unique_ptr<double> Parser::parse<double>(
  std::string str
) {
  std::unique_ptr<double> result = std::make_unique<double>(std::stod(str.c_str()));

  return std::move(result);
}

template<> std::unique_ptr<int> Parser::parse<int>(
  std::string str
) {
  std::unique_ptr<int> result = std::make_unique<int>(std::stoi(str.c_str()));

  return std::move(result);
}

template<> std::unique_ptr<std::string> Parser::parse<std::string>(
  std::string str
) {
  return std::make_unique<std::string>(str);
}

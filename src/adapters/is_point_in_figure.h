#pragma once

#include "../domain/figure.h"

namespace adapters {
  inline bool is_point_in_figure(
    std::unique_ptr<domain::Point> point,
    std::unique_ptr<domain::Figure> figure
  ) {
    return figure->has_point(std::move(point));
  }
}

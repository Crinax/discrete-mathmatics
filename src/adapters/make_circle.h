#pragma once

#include <memory>
#include "../domain/figure.h"
#include "../domain/circle.h"

namespace adapters {
  inline std::unique_ptr<domain::Circle> make_circle(
    double radius,
    std::unique_ptr<domain::Point> center
  ) {
    
    std::unique_ptr<domain::Circle> result =
      std::make_unique<domain::Circle>(domain::Circle { radius, std::move(center) });

    return std::move(result);
  }
}

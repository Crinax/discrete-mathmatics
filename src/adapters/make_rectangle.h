#pragma once

#include <memory>
#include "../domain/figure.h"
#include "../domain/rect.h"

namespace adapters {
  inline std::unique_ptr<domain::Rectangle> make_rectangle(
    double width,
    double height,
    std::unique_ptr<domain::Point> center
  ) {
    std::unique_ptr<domain::Rectangle> result =
      std::make_unique<domain::Rectangle>(domain::Rectangle { width, height, std::move(center) });

    return std::move(result);
  }
}

#pragma once

#include <memory>
#include "../domain/figure.h"

namespace adapters {
  inline std::unique_ptr<domain::Point> make_point(double x, double y) {
    std::unique_ptr<domain::Point> result =
      std::make_unique<domain::Point>(domain::Point { x, y });

    return std::move(result);
  }
}

#include <iostream>
#include <memory>
#include <regex>
#include "../console/prompt.h"
#include "../console/clear.h"
#include "pages.h"
#include "../variables.h"

void pages::input_circle() {
  console::clear();

  while (true) {
    std::unique_ptr<std::string> value =
      console::prompt<std::string>("Введите параметры круга в формате (radius, x, y): ");

    std::regex point_regex(R"(^\([+-]?([0-9]*[.])?[0-9]+, [+-]?([0-9]*[.])?[0-9]+, [+-]?([0-9]*[.])?[0-9]+\)$)");

    if (std::regex_match(value->data(), point_regex)) {
      app::figures.push_back(value->data());
      return pages::switch_page(1);
    }

    console::clear_line();
  }
}

#pragma once

#include <memory>
#include <vector>
#include <functional>

namespace pages {
  void main_page();
  void input_point();
  void choose_figure();
  void input_circle();
  void input_rectangle();
  void check_point();
  // void clear_figures();
  // void remove_point();

  template<typename... Args>
  void switch_page(int page, Args... args) {
    switch (page) {
      case -2:
        pages::input_circle(args...);
        break;

      case -1:
        pages::input_rectangle(args...);
        break;

      case 0:
        pages::main_page(args...);
        break;

      case 1:
        pages::choose_figure(args...);
        break;

      case 2:
        pages::input_point(args...);
        break;

      case 3:
        pages::check_point();
        break;

      // case 4:
      //   pages::clear_figures();
      //   break;
      //
      // case 5:
      //   pages::remove_point();
      //   break;
    }
  }
}

#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include "../console/prompt.h"
#include "../console/clear.h"
#include "../helpers/vector_join/vector_join.h"
#include "pages.h"

void pages::choose_figure() {
  console::clear();

  std::vector<std::string> points = {
    "1. Ввести параметры круга",
    "2. Ввести параметры прямоугольника",
    "3. Назад"
  };

  std::cout << helpers::vector_join(points) << std::endl << std::endl;

  std::unique_ptr<int> page;
  
  page = console::prompt<int>("Выберите меню: ");

  return pages::switch_page(*page - 3);
}

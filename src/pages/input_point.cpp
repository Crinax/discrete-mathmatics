#include <iostream>
#include <memory>
#include <regex>
#include "../console/prompt.h"
#include "../console/clear.h"
#include "pages.h"
#include "../variables.h"

void pages::input_point() {
  console::clear();

  while (true) {
    app::point_str = *console::prompt<std::string>("Введите координаты точки в формате (x, y): ").get();

    std::regex point_regex(R"(^\([+-]?([0-9]*[.])?[0-9]+, [+-]?([0-9]*[.])?[0-9]+\)$)");

    if (std::regex_match(app::point_str.data(), point_regex)) {
      return pages::switch_page(0);
    }

    console::clear_line();
  }
}

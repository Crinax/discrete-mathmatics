#include <iostream>
#include <memory>
#include "../helpers/vector_join/vector_join.h"
#include "../helpers/count_symbols/count_symbols.h"
#include "../variables.h"
#include "pages.h"
#include "../adapters/is_point_in_figure.h"
#include "../adapters/make_point.h"
#include "../adapters/make_rectangle.h"
#include "../adapters/make_circle.h"
#include "../domain/figure.h"
#include "../domain/rect.h"
#include "../domain/circle.h"
#include "../parser/parser.h"
#include "../console/clear.h"
#include "../predicates/variant_4.h"

template<> std::unique_ptr<domain::Point> Parser::parse<domain::Point>(
  std::string str
) {
  std::string params = str.substr(1, str.size() - 2);

  int separator_index = params.find(",");

  double x = std::stod(params.substr(0, separator_index));
  double y = std::stod(params.substr(separator_index + 1, std::string::npos));

  return std::make_unique<domain::Point>(domain::Point { x, y });
}

template<> std::unique_ptr<domain::Circle> Parser::parse<domain::Circle>(
  std::string str
) {
  std::string params = str.substr(1, str.size() - 2);
  int first_sep_index = params.find(",");
  std::string sec_part_params = params.substr(first_sep_index + 1, std::string::npos);
  int sec_sep_index = sec_part_params.find(",");

  double radius = std::stod(params.substr(0, first_sep_index));
  double x = std::stod(sec_part_params.substr(0, sec_sep_index));
  double y = std::stod(sec_part_params.substr(sec_sep_index + 1, std::string::npos));

  return std::make_unique<domain::Circle>(domain::Circle {
    radius,
    std::make_unique<domain::Point>(domain::Point { x, y })
  });
}

template<> std::unique_ptr<domain::Rectangle> Parser::parse<domain::Rectangle>(
  std::string str
) {
  std::string params = str.substr(1, str.size() - 2);
  int first_sep_index = params.find(",");
  std::string sec_part_params = params.substr(first_sep_index + 1, std::string::npos);
  int sec_sep_index = sec_part_params.find(",");
  std::string thr_part_params = sec_part_params.substr(sec_sep_index + 1, std::string::npos);
  int thr_sep_index = thr_part_params.find(",");

  double width = std::stod(params.substr(0, first_sep_index));
  double height = std::stod(sec_part_params.substr(0, sec_sep_index));
  double x = std::stod(thr_part_params.substr(0, thr_sep_index));
  double y = std::stod(thr_part_params.substr(thr_sep_index + 1, std::string::npos));

  return std::make_unique<domain::Rectangle>(domain::Rectangle {
    width,
    height,
    std::make_unique<domain::Point>(domain::Point { x, y })
  });
}

void pages::check_point() {
  console::clear();

  std::string wait;

  std::cout << "Point" << app::point_str << std::endl;
  std::cout << "\nFigures: " << "\n\t" <<
    helpers::vector_join(app::figures, "\n\t") << std::endl;

  Parser parser = {};
  std::unique_ptr<domain::Point> point = parser.parse<domain::Point>(app::point_str);
  std::vector<std::unique_ptr<domain::Figure>> figures = {};

  for (auto figure : app::figures) {
    if (helpers::count_symbols(figure, ',') == 2) {
      figures.push_back(parser.parse<domain::Circle>(figure));
    }

    if (helpers::count_symbols(figure, ',') == 3) {
      figures.push_back(parser.parse<domain::Rectangle>(figure));
    }
  }

  if (predicates::is_set_has_point(*point.get(), std::move(figures))) {
    std::cout << "Точка принадлежит множеству" << std::endl;
  } else {
    std::cout << "Точка НЕ принадлежит множетсву ¯\\_(ツ)_/¯" << std::endl;
  }

  std::cout << "Нажмите <Enter>, чтобы вернуться...";
  std::getline(std::cin, wait);

  return pages::switch_page(0);
}

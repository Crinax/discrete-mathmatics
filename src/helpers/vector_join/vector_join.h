#pragma once

#include <vector>
#include <sstream>
#include <string>
#include <iostream>

namespace helpers {
  template<typename TValue>
  std::string vector_join(std::vector<TValue> vec, std::string sep = "\n") {
    int vec_size = vec.size();
    std::stringstream result;
    for (int index = 0; index < vec_size; index++) {
      result << vec.at(index);
      if (vec_size - index > 1) {
        result << sep;
      }
    }

    return result.str();
  }
}

#pragma once

#include <string>

namespace helpers {
  inline std::string::size_type count_symbols(std::string input, char symbol) {
    std::string::size_type result = 0;

    for (std::string::size_type i = 0; i < input.size(); i++) {
      if (input.at(i) == symbol) {
        result++;
      }
    }

    return result;
  }
}

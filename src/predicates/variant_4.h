#pragma once

#include <memory>
#include <stdexcept>
#include <vector>
#include <iostream>
#include "../domain/figure.h"

namespace predicates {
  inline bool is_set_has_point(
    domain::Point point,
    std::vector<std::unique_ptr<domain::Figure>> figures
  ) {
    std::vector<bool> is_figures_has_point = {};

    for (int i = 0; i < figures.size(); i++) {
      is_figures_has_point.push_back(figures.at(i)->has_point(std::make_unique<domain::Point>(point)));
    }

    if (is_figures_has_point.size() < 4) {
      throw std::invalid_argument("Need 4 figures or more");
    }

    bool a = is_figures_has_point.at(0);
    bool b = is_figures_has_point.at(1);
    bool c = is_figures_has_point.at(2);
    bool d = is_figures_has_point.at(3);
    bool result = ((a && b) || !c) && d;

    return result;
  }
}

